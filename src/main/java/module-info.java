module making.gif {
    requires javafx.controls;
    requires javafx.web;

    requires java.base;
    requires java.naming;

    requires java.desktop;
    requires jdk.jsobject;
    requires org.deepsymmetry.wayang;
    requires javafx.swing;

    exports com.example.makinggif to javafx.graphics;
}