package com.example.makinggif.gif;

import org.deepsymmetry.GifSequenceWriter;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class GifCreator {

    public File convertImagesToGif(List<File> images, int delayTime, String outputGifPath) {
        createGif(images.stream().map(File::getAbsolutePath).collect(Collectors.toList()), outputGifPath, delayTime);
        return new File(outputGifPath);
    }

    private void createGif(List<String> imagePaths, String outputGifPath, int delayTime) {
        try {
            List<BufferedImage> images = imagePaths.stream().map(path -> {
                try {
                    return ImageIO.read(new File(path));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }).collect(Collectors.toList());
            Dimension maxDimension = findMaxDimensions(images);
            List<BufferedImage> resizedImages = images.stream().map(image ->
                    resizeImageToMaxDimensions(image, maxDimension)
            ).collect(Collectors.toList());

            BufferedImage firstImage = resizedImages.get(0);
            ImageOutputStream output = new FileImageOutputStream(new File(outputGifPath));
            GifSequenceWriter gifWriter = new GifSequenceWriter(output, firstImage.getType(), delayTime, true);
            gifWriter.writeToSequence(firstImage);

            for (int i = 1; i < resizedImages.size(); i++) {
                gifWriter.writeToSequence(resizedImages.get(i));
            }

            gifWriter.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Dimension findMaxDimensions(List<BufferedImage> images) {
        int maxWidth = 0;
        int maxHeight = 0;
        for (BufferedImage image : images) {
            if (image.getWidth() > maxWidth) {
                maxWidth = image.getWidth();
            }
            if (image.getHeight() > maxHeight) {
                maxHeight = image.getHeight();
            }
        }
        return new Dimension(maxWidth, maxHeight);
    }

    private BufferedImage resizeImageToMaxDimensions(BufferedImage originalImage, Dimension maxDimension) {
        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        BufferedImage resizedImage = new BufferedImage(maxDimension.width, maxDimension.height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, maxDimension.width, maxDimension.height, null);
        g.dispose();
        return resizedImage;
    }
}
