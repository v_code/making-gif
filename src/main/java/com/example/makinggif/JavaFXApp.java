package com.example.makinggif;

import com.example.makinggif.gif.GifCreator;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class JavaFXApp extends Application {
    private final GifCreator gifCreator = new GifCreator();
    private List<File> selectedImages = new ArrayList<>();
    private final HBox imagesContainer = new HBox(10);
    private TextField delayInput;
    private final int defaultValueForDelay = 500;
    private ImageView gifImageView;

    Button chooseFilesButton;
    Button makeGifButton;
    Button resetButton;

    private static final int PREVIEW_WIDTH = 200;
    private static final int PREVIEW_HEIGHT = 200;
    private Label statusLabel;
    private Locale currentLocale;
    private ComboBox<String> languageDropdown;
    Label title;
    Text instructions;
    Label delayLabel;
    File translationFile;
    private static final String LANGUAGE_FILE = "language.txt";

    @Override
    public void start(Stage primaryStage) {
        if(isFileMissing()) {
            switchToEnglish();
        } else {
            switch (readLanguageFromFile()) {
                case "en":
                    switchToEnglish();
                    break;
                case "ru":
                    switchToRussian();
                    break;
                case "ua":
                    switchToUkrainian();
                    break;
            }
        }

        int marginSize = 20;
        VBox root = new VBox(marginSize);
        root.setPadding(new Insets(marginSize));

        buttonInitialization();

        HBox buttonsBox = new HBox(marginSize);
        buttonsBox.setAlignment(Pos.CENTER_LEFT);
        buttonsBox.getChildren().addAll(chooseFilesButton, makeGifButton);

        VBox leftBox = new VBox(marginSize);
        leftBox.getChildren().addAll(setTitle(), setInstructions(), buttonsBox);

        HBox delayBox = new HBox(marginSize);
        delayBox.setAlignment(Pos.CENTER_LEFT);
        delayLabel = new Label(getTranslate("delayLabel"));
        delayLabel.getStyleClass().add("label-delay");

        TextField delayInput = setDelay();
        delayBox.getChildren().addAll(delayLabel, delayInput,
                resetButton, createLanguageDropdown());

        gifImageView = new ImageView();
        gifImageView.setFitWidth(PREVIEW_WIDTH);
        gifImageView.setFitHeight(PREVIEW_HEIGHT);
        gifImageView.setPreserveRatio(true);
        VBox rightBox = new VBox(marginSize);
        rightBox.getChildren().addAll(delayBox, gifImageView);

        HBox topLayout = new HBox(20);
        HBox.setHgrow(leftBox, Priority.ALWAYS);
        topLayout.getChildren().addAll(leftBox, rightBox);

        VBox imageBox = new VBox(marginSize);
        ScrollPane scrollPane = new ScrollPane(imagesContainer);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setPrefSize(400, 300);
        imageBox.getChildren().addAll(createStatusBar(), scrollPane );

        root.getChildren().addAll(topLayout, imageBox);
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.setTitle("GIF Creator");

        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            if(isFileMissing()) {
                try {
                    translationFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String valueFromFile = readLanguageFromFile();
            String value = languageDropdown.getValue();
            if (!value.equals(valueFromFile)){
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(LANGUAGE_FILE))) {
                    writer.write(value);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean isFileMissing() {
        translationFile = new File(LANGUAGE_FILE);
        return !translationFile.exists();
    }

    private String readLanguageFromFile() {
        String path = "language.txt";
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "en";
        }
    }

    private ComboBox<String> createLanguageDropdown() {
        languageDropdown = new ComboBox<>();
        List<String> languages = Arrays.asList("en", "ru", "ua");
        languageDropdown.getItems().addAll(languages);
        languageDropdown.setValue(currentLocale.getLanguage());
        languageDropdown.setOnAction(event -> {
            String selectedLanguage = languageDropdown.getValue();
            switch (selectedLanguage) {
                case "en":
                    switchToEnglish();
                    break;
                case "ru":
                    switchToRussian();
                    break;
                case "ua":
                    switchToUkrainian();
                    break;
            }
            updateInterface();
        });
        return languageDropdown;
    }

    private void updateInterface() {
        title.setText(getTranslate("title"));
        instructions.setText(getTranslate("instructions"));
        chooseFilesButton.setText(getTranslate("chooseFilesButton"));
        makeGifButton.setText(getTranslate("makeGifButton"));
        resetButton.setText(getTranslate("resetButton"));
        delayLabel.setText(getTranslate("delayLabel"));
        statusLabel.setText(getTranslate("infoText"));
    }

    private BorderPane createStatusBar() {
        BorderPane statusBar = new BorderPane();
        statusLabel = new Label(getTranslate("infoText"));
        statusBar.setLeft(statusLabel);
        statusBar.getStyleClass().add("second-text");
        return statusBar;
    }

    public void setStatusMessage(String message) {
        statusLabel.setText(message);
    }

    private TextField setDelay() {
        delayInput  = new TextField();
        delayInput.setPromptText(String.valueOf(defaultValueForDelay));
        delayInput.getStyleClass().addAll("text-field-style", "delay-input");
        return delayInput;
    }

    private Label setTitle() {
        title = new Label(getTranslate("title"));
        title.getStyleClass().add("text-title");
        return title;
    }

    private Text setInstructions() {
        String instructionsText = getTranslate("instructions");
        instructions = new Text(instructionsText);
        instructions.getStyleClass().add("second-text");
        return instructions;
    }

    private String getTranslate(String key) {
        try (InputStream is = getClass().getResourceAsStream("/translations_" + currentLocale.getLanguage() + ".xml")) {
            XMLResourceBundle bundle = new XMLResourceBundle(is);
            return bundle.getString(key);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void switchToEnglish() {
        currentLocale = new Locale("en", "US");
    }

    public void switchToRussian() {
        currentLocale = new Locale("ru", "RU");
    }

    public void switchToUkrainian() {
        currentLocale = new Locale("ua", "UA");
    }

    private void buttonInitialization() {
        chooseFilesButton = new Button(getTranslate("chooseFilesButton"));
        chooseFilesButton.getStyleClass().addAll("button-style", "button-default");
        chooseFilesButton.setOnMouseEntered(e -> chooseFilesButton.getStyleClass().add("button-hover"));
        chooseFilesButton.setOnMouseExited(e -> chooseFilesButton.getStyleClass().remove("button-hover"));
        chooseFilesButton.setOnAction(e -> chooseFiles());

        makeGifButton = new Button(getTranslate("makeGifButton"));
        makeGifButton.getStyleClass().addAll("button-style", "button-make-gif-default");
        makeGifButton.setOnMouseEntered(e -> makeGifButton.getStyleClass().add("button-make-gif-hover"));
        makeGifButton.setOnMouseExited(e -> makeGifButton.getStyleClass().remove("button-make-gif-hover"));
        makeGifButton.setOnAction(e -> makeGif());

        resetButton = new Button(getTranslate("resetButton"));
        resetButton.getStyleClass().addAll("button-style-second", "button-reset-default");
        resetButton.setOnMouseEntered(e -> resetButton.getStyleClass().add("button-reset-hover"));
        resetButton.setOnMouseExited(e -> resetButton.getStyleClass().remove("button-reset-hover"));
        resetButton.setOnAction(e -> resetApp());
    }

    private void resetApp() {
        delayInput.setText("");
        delayInput.setPromptText(String.valueOf(defaultValueForDelay));
        imagesContainer.getChildren().clear();
        selectedImages.clear();
        gifImageView.setImage(null);
        statusLabel.setText(getTranslate("infoText"));
    }

    private void chooseFiles() {
        statusLabel.setText(getTranslate("infoText"));
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg"));
        List<File> files = fileChooser.showOpenMultipleDialog(null);
        if (files != null && !files.isEmpty()) {
            if (selectedImages == null) {
                selectedImages = new ArrayList<>();
            }
            selectedImages.addAll(files);
            imagesContainer.getChildren().clear();
            for (File file : selectedImages) {
                try {
                    Image image = new Image(new FileInputStream(file));
                    ImageView imageView = new ImageView(image);
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);
                    imageView.setOnMouseClicked(e -> {
                        ContextMenu contextMenu = new ContextMenu();
                        MenuItem removeItem = new MenuItem("Remove");
                        removeItem.setOnAction(event -> {
                            imagesContainer.getChildren().remove(imageView);
                            selectedImages.remove(file);
                        });
                        contextMenu.getItems().add(removeItem);
                        contextMenu.show(imageView, e.getScreenX(), e.getScreenY());
                    });
                    imagesContainer.getChildren().add(imageView);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void makeGif() {
        if (selectedImages != null && !selectedImages.isEmpty()) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("GIF Files", "*.gif"));
            fileChooser.setInitialFileName("result.gif");
            File outputFile = fileChooser.showSaveDialog(null);
            int delay;
            try {
                delay = Integer.parseInt(delayInput.getText());
            } catch (NumberFormatException e) {
                delay = defaultValueForDelay;
            }
            if (outputFile != null) {
                gifCreator.convertImagesToGif(selectedImages, delay, outputFile.getAbsolutePath());
                playGif(outputFile.getAbsolutePath());
                openFileLocation(outputFile);
                setStatusMessage(getTranslate("gifCreationSuccess"));
            } else {
                setStatusMessage(getTranslate("gifCreationCancelled"));
            }
        } else {
            setStatusMessage(getTranslate("selectImagesFirst"));
        }
    }

    private void playGif(String gifPath) {
        try {
            Image gif = new Image(new FileInputStream(gifPath));
            gifImageView.setImage(gif);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openFileLocation(File file) {
        try {
            Desktop.getDesktop().open(file.getParentFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}