# GIF Creator
A JavaFX application for creating GIFs from user-selected pictures.

![screenshot Making GIF app](img/screen_400.png)

## Features

- Select multiple images to create a GIF.
- Set the delay between frames.
- View selected images before creating a GIF.
- Delete unnecessary pictures before creating animation.
- Saving the finished GIF on your computer.
- Displaying the created GIF.
- Language selection and saving.
- Show informational message.
- Show scrolling with a large number of images.
- Resizing images before creating GIF.

> Naming the file and selecting a save location.
> Opens the file folder after making a GIF.
> Reset the application to its original state.

## Technologies

- [Java](https://www.oracle.com/cis/java/technologies/downloads/) v.17 or higher.
- Maven.
- JavaFX 16.
- XML.
- CSS.

## Installation

- Download the project repository.
- Make sure you have the required technologies installed.
- Run ```MakingGifApplication.java``` to run the application, or create a jar artifact, or run making-gif.jar (in the out folder).

*The language.txt file will be created in the same folder as the jar, don't delete it.*

### Usage
1. Click on the ```Choose Files``` button and select the images you want to use.
2. Delete an extra picture, if there is one, by right-clicking on it and selecting ```Remove```.
3. Set the delay between frames by entering the desired value in the field.
4. Click on the ```Make GIF``` button and select a save location.
5. Ready! Your GIF will be created.
6. Click on the ```Reset``` button to reset all values.

### History

Initially, the project was created for the web version on Spring. 
At the moment, this is a fully desktop application.